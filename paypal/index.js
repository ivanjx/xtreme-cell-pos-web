function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

// Getting query params.
$('[name=business]').val(getParameterByName('account_id'));
$('[name=item_name]').val(getParameterByName('item_name'));
$('[name=a3]').val(getParameterByName('amount'));
$('[name=t3]').val(getParameterByName('period'));
$('[name=custom]').val(getParameterByName('user_id'));
$('[name=notify_url]').val(getParameterByName('ipn_url'));

// Submitting form.
$('#frm').submit();