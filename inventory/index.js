// View model.
function AppViewModel() {
    let self = this;

    self.inventories = ko.observableArray();
    self.inventoriesSort = {
        sortField: null,
        ascending: true
    }

    self.sortInventories = function(column) {
        if(isLoadingInventories) {
            return;
        }

        if(column != self.inventoriesSort.sortField) {
            // Change entire sorting.
            self.inventoriesSort.sortField = column;
            self.inventoriesSort.ascending = true;
        } else {
            // Just change direction.
            self.inventoriesSort.ascending = !self.inventoriesSort.ascending;
        }

        // Reloading list.
        loadInventories(true);
    }

    self.inventoryEdit = function(inventory) {

    }
}

// Functions.
function loadInventories(initial) {
    // Prevent from loading multiple times at once.
    if(isLoadingInventories) {
        return;
    }

    if(initial) {
        // Clearing items.
        vm.inventories.removeAll();
        canLoadMoreInventories = true;
    } else if(!initial && !canLoadMoreInventories) {
        return;
    }

    isLoadingInventories = true;
    $('#lblLoading').html('Loading...');

    // Preparing data.
    let reqData = {
        filter: $('#txtSearch').val(),
        skip: vm.inventories().length,
        sortField: vm.inventoriesSort.sortField,
        ascending: vm.inventoriesSort.ascending
    };
    
    // Sending request.
    getJson(API_URL + '/inventory/list', reqData, function(err, response) {
        $('#lblLoading').html('');
        isLoadingInventories = false;

        if(err) {
            openMessageModal('Error', 'Unable to load data.<br/>Details: ' + err);
            return;
        }

        if(!response.success) {
            openMessageModal('Error', 'Unable to load data.<br/>Details: ' + response.message);
            return;
        }

        for(let i = 0; i < response.inventories.length; ++i) {
            // Checking for duplicate.
            let duplicates = vm.inventories().filter(inventory => inventory.id == response.inventories[i].id);

            if(duplicates.length > 0) {
                // This is maybe caused by the pagination but it is not a big deal.
                continue;
            }

            // Adding to the array.
            let inventory = new InventoryModel();
            inventory.id(response.inventories[i].id);
            inventory.name(response.inventories[i].name);

            vm.inventories.push(inventory);
        }

        if(response.inventories.length == 0) {
            // No more items to load.
            canLoadMoreInventories = false;
            return;
        }

        if($(".item-container").prop('scrollHeight') > $(".item-container").height()) { // true if overflowing.
           // Is already overflowing.
           // Will load more at scroll to bottom event down below.
           return;
        }

        // Getting more items.
        loadInventories(false);
    });
}

// Entry point.
let canLoadMoreInventories;
let isLoadingInventories = false;
let inventoryBeingEdited;

// Applying bindings.
let vm = new AppViewModel();
ko.applyBindings(vm);

// Disable sidebar item.
$('#sidebarInventory').addClass('active');
$('#sidebarInventory').removeAttr('href');

// Checking login info.
getUserInfo(function(err, userInfo) {
    if(err) {
        // Go to main router.
        window.location = '/';
    }

    // Starting auto token refresher.
    startAutoRefreshToken();

    // Loading items.
    loadInventories(true);
});

// Events.
$('#btnSearch').on('click', function() {
    loadInventories(true);
});

// Scroll to bottom event.
$('.item-container').scroll(function(event) {
    var element = event.target;

    if (element.scrollHeight - element.scrollTop === element.clientHeight)
    {
        // Reached the bottom here.
        // Getting more items if any.
        loadInventories(false);
    }
});