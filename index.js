// Checking login status.
getUserInfo(function(err, userInfo) {
    if(err) {
        // Need to login.
        window.location = '/login';
    } else {
        // Go to home.
        window.location = '/home';
    }
});