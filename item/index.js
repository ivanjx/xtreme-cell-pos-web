// View model.
function AppViewModel() {
    let self = this;

    self.items = ko.observableArray();
    self.itemsSort = {
        sortField: null,
        ascending: true
    };

    self.sortItems = function(column) {
        if(isLoadingItems) {
            return;
        }

        if(column != self.itemsSort.sortField) {
            // Change entire sorting.
            self.itemsSort.sortField = column;
            self.itemsSort.ascending = true;
        } else {
            // Just change direction.
            self.itemsSort.ascending = !self.itemsSort.ascending;
        }

        // Reloading list.
        loadItems(true);
    }

    self.itemEdit = function(item) {
        itemBeingEdited = item;

        // Filling input.
        $('#txtId_itemContainer').val(item.customId());
        $('#txtName_itemContainer').val(item.name());
        $('#txtDescription_itemContainer').val(item.description());

        // Opening item modal.
        openItemModal(false);
    }
}

// Functions.
function loadItems(initial) {
    // Prevent from loading multiple times at once.
    if(isLoadingItems) {
        return;
    }

    if(initial) {
        // Clearing items.
        vm.items.removeAll();
        canLoadMoreItems = true;
    } else if(!initial && !canLoadMoreItems) {
        return;
    }

    isLoadingItems = true;
    $('#lblLoading').html('Loading...');

    // Preparing data.
    let reqData = {
        filter: $('#txtSearch').val(),
        skip: vm.items().length,
        sortField: vm.itemsSort.sortField,
        ascending: vm.itemsSort.ascending
    };
    
    // Sending request.
    getJson(API_URL + '/item/list', reqData, function(err, response) {
        $('#lblLoading').html('');
        isLoadingItems = false;

        if(err) {
            openMessageModal('Error', 'Unable to load data.<br/>Details: ' + err);
            return;
        }

        if(!response.success) {
            openMessageModal('Error', 'Unable to load data.<br/>Details: ' + response.message);
            return;
        }

        for(let i = 0; i < response.items.length; ++i) {
            // Checking for duplicate.
            let duplicates = vm.items().filter(item => item.id == response.items[i].id);

            if(duplicates.length > 0) {
                // This is maybe caused by the pagination but it is not a big deal.
                continue;
            }

            // Adding to the array.
            let item = new ItemModel();
            item.id(response.items[i].id);
            item.customId(response.items[i].customId);
            item.name(response.items[i].name);
            item.description(response.items[i].description);

            vm.items.push(item);
        }

        if(response.items.length == 0) {
            // No more items to load.
            canLoadMoreItems = false;
            return;
        }

        if($(".item-container").prop('scrollHeight') > $(".item-container").height()) { // true if overflowing.
           // Is already overflowing.
           // Will load more at scroll to bottom event down below.
           return;
        }

        // Getting more items.
        loadItems(false);
    });
}

function openItemModal(isNew) {
    // Setting up modal.
    if(isNew) {
        // Hiding delete button.
        $('#btnDelete_itemContainer').css('display', 'none');
    } else {
        // Showing delete button.
        $('#btnDelete_itemContainer').css('display', 'inline-block');
    }

    // Opening modal.
    showElement($('#itemContainer'));
    $('#itemContainer .popup-content').css('transform', 'scale(1)');
}

function closeItemModal() {
    // Clearing inputs.
    $('#txtId_itemContainer').val('');
    $('#txtName_itemContainer').val('');
    $('#txtDescription_itemContainer').val('');

    // Closing modal.
    $('#itemContainer .popup-content').css('transform', 'scale(0)');
    
    setTimeout(() => {
        hideElement($('#itemContainer'));
    }, 300);
}

// Entry point.
// Global variables.
let isLoadingItems = false;
let canLoadMoreItems = true;
let itemBeingEdited;

// Applying bindings.
let vm = new AppViewModel();
ko.applyBindings(vm);

// Disable sidebar item.
$('#sidebarItem').addClass('active');
$('#sidebarItem').removeAttr('href');

// Checking login info.
getUserInfo(function(err, userInfo) {
    if(err) {
        // Go to main router.
        window.location = '/';
    }

    // Starting auto token refresher.
    startAutoRefreshToken();

    // Loading items.
    loadItems(true);
});

// Events.
$('#btnSearch').on('click', function() {
    loadItems(true);
});

$('#btnAddItem').on('click', function() {
    openItemModal(true);
});

$('#frmItem').on('submit', function(e) {
    // Preventing page reloads.
    e.preventDefault();

    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Getting input values.
    let customId = $('#txtId_itemContainer').val();
    let name = $('#txtName_itemContainer').val();
    let description = $('#txtDescription_itemContainer').val();

    // Determining api path.
    let url = API_URL;

    if(itemBeingEdited) { // if editing
        url += '/item/update';
    } else {
        url += '/item/add';
    }

    // Preparing data.
    let reqData = {
        id: itemBeingEdited ? itemBeingEdited.id() : null,
        customId: customId,
        name: name,
        description: description
    };

    // Sending request.
    postJson(url, reqData, function(err, response) {
        // Hiding loading animation.
        $('#spinnerContainer').css('display', 'none');

        if(err) {
            openMessageModal('Error', 'Unable to contact the server.<br/>Details: ' + err);
            return;
        }

        if(!response.success) {
            openMessageModal('Error', 'Unable to save changes.<br/>Details: ' + response.message);
            return;
        }

        // Closing modal.
        closeItemModal();

        // Reloads the whole table.
        loadItems(true);
    });
});

$('#btnCancel_itemContainer').on('click', function() {
    closeItemModal();
});

$('#btnDelete_itemContainer').on('click', function() {
    // Prompting user.
    openMessageModal('Confirmation', 'Are you sure want to delete selected item?', true);

    messageModalCallback = function(isOk) {
        messageModalCallback = null;

        if(!isOk) {
            return;
        }

        // Showing loading animation.
        $('#spinnerContainer').css('display', 'block');

        // Preparing request.
        let reqData = {
            id: itemBeingEdited.id()
        };

        // Sending request.
        postJson(API_URL + '/item/delete', reqData, function(err, response) {
            // Stopping loading animation.
            $('#spinnerContainer').css('display', 'none');

            if(err) {
                openMessageModal('Error', 'Unable to contact server.\nDetails: ' + err);
                return;
            }

            if(!response.success) {
                openMessageModal('Error', 'Unable to delete selected item.\nDetails: ' + response.message);
                return;
            }

            // Closing item modal.
            closeItemModal();

            // Deleting from view model.
            vm.items.remove(itemBeingEdited);
        });
    }
});

// Scroll to bottom event.
$('.item-container').scroll(function(event) {
    var element = event.target;

    if (element.scrollHeight - element.scrollTop === element.clientHeight)
    {
        // Reached the bottom here.
        // Getting more items if any.
        loadItems(false);
    }
});