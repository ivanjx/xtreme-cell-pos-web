// Functions definition.
function openRegTokenModal() {
    // Opening modal.
    showElement($('#regTokenContainer'));
    $('#regTokenContainer .popup-content').css('transform', 'scale(1)');

    // Focusing input.
    $('#txtRegToken').focus();
}

function closeRegTokenModal() {
    $('#regTokenContainer .popup-content').css('transform', 'scale(0)');
    
    setTimeout(() => {
        hideElement($('#regTokenContainer'));
    }, 300);
}

// Entry point.
// Checking login status.
getUserInfo(function(err, userInfo) {
    if(!err) {
        // Go to home page.
        window.location = '/home';
    }
});

// Events.
$('#btnCancel_regTokenContainer').on('click', function() {
    closeRegTokenModal();
});

$('#frmRegister').on('submit', function(e) {
    // Preventing page reloads.
    e.preventDefault();

    // Validating password.
    let password = $('#txtPassword').val();
    let confirmPassword = $('#txtConfirmPassword').val();

    if(password != confirmPassword) {
        openMessageModal('Error', 'Password confirmation does not match');
        return;
    }

    password = confirmPassword = null;

    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Checking email availability.
    // Preparing data.
    let reqData = {
        email: $('#txtEmail').val()
    };

    // Sending request.
    postJson(API_URL + '/user/check-email-availability', reqData, function(err, response) {
        if(err) {
            // Stopping loading animation.
            $('#spinnerContainer').css('display', 'none');

            // Showing error message.
            openMessageModal('Error', err);
        }

        if(!response.success) {
            // Stopping loading animation.
            $('#spinnerContainer').css('display', 'none');

            // Showing error message.
            openMessageModal('Error', response.message);

            return;
        }

        if(!response.isAvailable) {
            // Stopping loading animation.
            $('#spinnerContainer').css('display', 'none');

            // Showing error message.
            openMessageModal('Error', 'Email already used');

            return;
        }

        // Email is not used yet.
        // Requesting reg token.
        postJson(API_URL + '/user/send-reg-token', reqData, function(err, response) {
            // Stopping loading animation.
            $('#spinnerContainer').css('display', 'none');

            if(err) {
                // Showing error message.
                openMessageModal('Error', err);
                return;
            }

            if(!response.success) {
                openMessageModal('Error', response.message);
                return;
            }

            // Displaying success message.
            openMessageModal('Info', 'We have sent a registration token to your email. Please check it out!');

            messageModalCallback = function() {
                messageModalCallback = null;
                
                // Showing reg token input.
                openRegTokenModal();
            };
        });
    });
});

$('#frmSubmitRegister').on('submit', function(e) {
    // Preventing page reloads.
    e.preventDefault();

    // Validating.
    let regToken = $('#txtRegToken').val();

    if(!regToken) {
        openMessageModal('Error', 'Registration token is empty');
        return;
    }

    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Preparing data.
    let reqData = {
        regToken: regToken,
        email: $('#txtEmail').val(),
        plainPassword: $('#txtPassword').val(),
        storeName: $('#txtStoreName').val()
    };

    // Sending request.
    postJson(API_URL + '/user/register', reqData, function(err, response) {
        // Stopping loading animation.
        $('#spinnerContainer').css('display', 'none');

        if(err) {
            // Showing error message.
            openMessageModal('Error', err);
            return;
        }

        if(!response.success) {
            openMessageModal('Error', response.message);
            return;
        }

        // Showing success message.
        openMessageModal('Info', 'Your account has been created. You can now login with the new account.');

        messageModalCallback = function() {
            messageModalCallback = null;
            
            // Redirecting to login page.
            window.location = '/login';
        }
    });
});