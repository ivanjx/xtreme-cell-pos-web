// Global.
let m_userInfo;

// Functions definition.
function openEditAccountModal() {
    // Opening modal.
    showElement($('#editAccountContainer'));
    $('#editAccountContainer .popup-content').css('transform', 'scale(1)');
}

function closeEditAccountModal() {
    // Clearing user inputs.
    $('#txtStoreName_editAccountContainer').val('');
    $('#txtEmail_editAccountContainer').val('');
    $('#txtCurrentPassword_editAccountContainer').val('');
    $('#txtNewPassword_editAccountContainer').val('');
    $('#txtConfirmPassword_editAccountContainer').val('');
    $('#txtReceiptFooter_editAccountContainer').val('');

    // Closing modal.
    $('#editAccountContainer .popup-content').css('transform', 'scale(0)');
    
    setTimeout(() => {
        hideElement($('#editAccountContainer'));
    }, 300);
}

function openEditEmailTokenModal() {
    // Opening modal.
    showElement($('#editEmailTokenContainer'));
    $('#editEmailTokenContainer .popup-content').css('transform', 'scale(1)');

    // Focusing input.
    $('#txtToken_editEmailTokenContainer').focus();
}

function closeEditEmailTokenModal() {
    // Clearing user inputs.
    $('#txtToken_editEmailTokenContainer').val('');

    // Closing modal.
    $('#editEmailTokenContainer .popup-content').css('transform', 'scale(0)');

    setTimeout(() => {
        hideElement($('#editEmailTokenContainer'));
    }, 300);
}

// Entry point.
// Disable sidebar item.
$('#sidebarHome').addClass('active');
$('#sidebarHome').removeAttr('href');

// Checking login status.
getUserInfo(function(err, userInfo) {
    if(err) {
        // Go to main router.
        window.location = '/';
    }

    // Starting auto refresh token.
    startAutoRefreshToken();
    
    // Filling info fields.
    $('#txtStoreName').text(userInfo.store.name);
    $('#txtEmail').text(userInfo.email);
    $('#txtSubscriptionStatus').text(userInfo.subscription.status);

    if(userInfo.subscription.status == 'ACTIVE') {
        let periodName;

        if(userInfo.subscription.periodType == 'D') {
            periodName = 'Daily';
        } else if(userInfo.subscription.periodType == 'M') {
            periodName = 'Monthly';
        }

        $('#trSubscriptionPeriod').css('display', '');
        $('#txtSubscriptionPeriod').text(periodName);

        $('#trSubscriptionAmount').css('display', '');
        $('#txtSubscriptionAmount').text(userInfo.subscription.amountPerPeriod);
    } else if(userInfo.subscription.status == 'STOPPED' && userInfo.subscription.expireTime > new Date()) {
        $('#trSubscriptionExpireTime').css('display', '');
        $('#txtSubscriptionExpireTime').text(moment(userInfo.subscription.expireTime).format('YYYY-MM-DD HH:mm:ss'));
    }
});

// Events.
$('#btnEdit').on('click', function() {
    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Getting user info.
    getUserInfo(function(err, userInfo) {
        // Stopping loading animation.
        $('#spinnerContainer').css('display', 'none');

        if(err) {
            // Go to main router.
            window.location = '/';
        }

        m_userInfo = userInfo;

        // Filling user inputs.
        $('#txtStoreName_editAccountContainer').val(userInfo.store.name);
        $('#txtEmail_editAccountContainer').val(userInfo.email);
        $('#txtReceiptFooter_editAccountContainer').val(userInfo.store.receiptFormat.footer);

        // Showing editor.
        openEditAccountModal();
    });
});

$('#btnCancel_editAccountContainer').on('click', function() {
    closeEditAccountModal();
});

$('#frmEditAccount').on('submit', function(e) {
    // Preventing page reloads.
    e.preventDefault();

    // Validating.
    let currentPassword = $('#txtCurrentPassword_editAccountContainer').val();
    let newPassword = $('#txtNewPassword_editAccountContainer').val();
    let confirmPassword = $('#txtConfirmPassword_editAccountContainer').val();

    if(newPassword) {
        // Validating passwords.
        if(!currentPassword) {
            openMessageModal('Error', 'Current password is empty');
            return;
        }

        if(newPassword != confirmPassword) {
            openMessageModal('Error', 'Passwords do not match');
            return;
        }
    }

    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Updating store info.
    // Preparing data.
    let reqData = {
        storeInfo: {
            name: $('#txtStoreName_editAccountContainer').val(),
            receiptFormat: {
                footer: $('#txtReceiptFooter_editAccountContainer').val()
            }
        }
    };

    // Sending request.
    postJson(API_URL + '/user/update/store', reqData, function(err, response) {
        if(err) {
            $('#spinnerContainer').css('display', 'none');
            openMessageModal('Error', err);
            return;
        }

        if(!response.success) {
            $('#spinnerContainer').css('display', 'none');
            openMessageModal('Error', response.message);
            return;
        }

        let updateEmail = function() {
            let email = $('#txtEmail_editAccountContainer').val();

            if(email != m_userInfo.email) {
                // Checking email availability.
                // Preparing data.
                let reqData = {
                    email: email
                };

                postJson(API_URL + '/user/check-email-availability', reqData, function(err, response) {
                    if(err) {
                        $('#spinnerContainer').css('display', 'none');
                        openMessageModal('Error', err);
                        return;
                    }
            
                    if(!response.success) {
                        $('#spinnerContainer').css('display', 'none');
                        openMessageModal('Error', response.message);
                        return;
                    }

                    if(!response.isAvailable) {
                        $('#spinnerContainer').css('display', 'none');
                        openMessageModal('Error', 'Email is already taken. Please use another one.');
                        return;
                    }

                    // Requesting token.
                    postJson(API_URL + '/user/send-reg-token', reqData, function(err, response) {
                        // Stopping loading animation.
                        $('#spinnerContainer').css('display', 'none');

                        if(err) {
                            openMessageModal('Error', err);
                            return;
                        }
                
                        if(!response.success) {
                            openMessageModal('Error', response.message);
                            return;
                        }

                        // Closing edit account container coz we dont need it no more.
                        closeEditAccountModal();

                        // Showing message.
                        openMessageModal('Info', 'To complete the email change process, you must enter the token that was just sent to your new email. Please check it out.');

                        messageModalCallback = function() {
                            messageModalCallback = null;

                            // Passing new email to a hidden input.
                            $('#txtEmail_editEmailTokenContainer').val(email);

                            // Opening token input modal.
                            openEditEmailTokenModal();
                        }
                    });
                });
            } else {
                // No need to change email.
                // All done.
                location.reload(false);
            }
        };

        // Updating password.
        if(newPassword) {
            // Preparing data.
            let reqData = {
                currentPlainPassword: currentPassword,
                newPlainPassword: newPassword
            };

            // Sending request.
            postJson(API_URL + '/user/update/password', reqData, function(err, response) {
                if(err) {
                    $('#spinnerContainer').css('display', 'none');
                    openMessageModal('Error', err);
                    return;
                }
        
                if(!response.success) {
                    $('#spinnerContainer').css('display', 'none');
                    openMessageModal('Error', response.message);
                    return;
                }

                // Updating email.
                updateEmail();
            });
        } else {
            // Updating email.
            updateEmail();
        }
    });
});

$('#frmEditEmailToken').on('submit', function(e) {
    // Prevent page reload.
    e.preventDefault();

    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Preparing data.
    let reqData = {
        email: $('#txtEmail_editEmailTokenContainer').val(),
        regToken: $('#txtToken_editEmailTokenContainer').val()
    };

    // Sending request.
    postJson(API_URL + '/user/update/email', reqData, function(err, response) {
        // Stopping loading animation.
        $('#spinnerContainer').css('display', 'none');

        if(err) {
            openMessageModal('Error', err);
            return;
        }

        if(!response.success) {
            openMessageModal('Error', response.message);
            return;
        }

        // All done.
        // Reloading page.
        location.reload(false);
    });
});

$('#btnCancel_editEmailTokenContainer').on('click', function() {
    // Reloading page to display the other changes.
    location.reload(false);
});

$('#btnSubscription').on('click', function() {
    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Getting user info.
    getUserInfo(function(err, userInfo) {
        if(err) {
            $('#spinnerContainer').css('display', 'none');
            openMessageModal('Error', err.message);
            return;
        }

        if(userInfo.subscription.status == 'ACTIVE') {
            // Stopping loading animation.
            $('#spinnerContainer').css('display', 'none');

            // Ask to cancel.
            openMessageModal('Subscription', 'Do you want to stop your subscription now?', true);

            messageModalCallback = function(isOk) {
                messageModalCallback = null;
                
                if(isOk) {
                    // Cancelling paypal subscription.
                    // Showing loading animation.
                    $('#spinnerContainer').css('display', 'block');

                    // Getting paypal account id.
                    getJson(API_URL + '/payment/subscription-info', null, function(err, response) {
                        // Stopping loading animation.
                        $('#spinnerContainer').css('display', 'none');

                        if(err) {
                            openMessageModal('Error', err);
                            return;
                        }

                        if(!response.success) {
                            openMessageModal('Error', response.message);
                            return;
                        }

                        // Opening cancel url.
                        window.open('https://www.paypal.com/cgi-bin/webscr?cmd=_subscr-find&alias='+ response.paypalAccountId + '&switch_classic=true')
                    });
                }
            };
        } else {
            // Getting latest subscription info.
            getJson(API_URL + '/payment/subscription-info', null, function(err, response) {
                // Stopping loading animation.
                $('#spinnerContainer').css('display', 'none');

                if(err) {
                    openMessageModal('Error', err);
                    return;
                }

                if(!response.success) {
                    openMessageModal('Error', response.message);
                    return;
                }

                let periodName;

                if(response.periodType == 'D') {
                    periodName = 'day';
                } else if(response.periodType == 'M') {
                    periodName = 'month';
                }

                // Asking to start subscription.
                openMessageModal('Subscription', 'Do you want to start subscription now?<br/>' + response.amountPerPeriod + ' USD per ' + periodName, true);

                messageModalCallback = function(isOk) {
                    messageModalCallback = null;

                    if(isOk) {
                        // Calling subscription page.
                        let paypalUrl;
                        paypalUrl = '/paypal/?' + 
                        'account_id=' + response.paypalAccountId +
                        '&item_name=' + 'Xtreme Cell POS (' + encodeURIComponent(userInfo.email) + ')' +
                        '&amount=' + encodeURIComponent(response.amountPerPeriod) +
                        '&period=' + response.periodType +
                        '&user_id=' + userInfo.id +
                        '&ipn_url=' + encodeURIComponent(response.paypalIpnUrl);

                        window.open(paypalUrl, '_blank');

                        // Showing info message.
                        openMessageModal('Info', 'After completing the payment, please refresh this page to see the changes.');

                        // Waiting for payment then reload.
                        window.onfocus = function() {
                            window.location.reload();
                        }
                    }
                };
            });
        }
    });
});