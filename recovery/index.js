function openTokenModal() {
    // Opening modal.
    showElement($('#tokenContainer'));
    $('#tokenContainer .popup-content').css('transform', 'scale(1)');

    // Focusing input.
    $('#txtToken').focus();
}

function closeTokenModal() {
    $('#tokenContainer .popup-content').css('transform', 'scale(0)');
    
    setTimeout(() => {
        hideElement($('#tokenContainer'));
    }, 300);
}

// Events.
$('#frmRecovery').on('submit', function(e) {
    // Preventing page reloads.
    e.preventDefault();

    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Preparing data.
    let email = $('#txtEmail').val();

    let reqData = {
        email: email
    };

    // Sending request.
    postJson(API_URL + '/user/send-reg-token', reqData, function(err, response) {
        // Stopping loading animation.
        $('#spinnerContainer').css('display', 'none');

        if(err) {
            // Showing error message.
            openMessageModal('Error', err);
            return;
        }

        if(!response.success) {
            openMessageModal('Error', response.message);
            return;
        }

        // Displaying success message.
        openMessageModal('Info', 'We have sent a token to your email. Please check it out!');

        messageModalCallback = function() {
            messageModalCallback = null;
            
            // Showing token input.
            openTokenModal();
        };
    });
})

$('#btnCancel_tokenContainer').on('click', function() {
    closeTokenModal();
});

$('#frmSubmitRecovery').on('submit', function(e) {
    // Preventing page reloads.
    e.preventDefault();

    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Preparing data.
    let email = $('#txtEmail').val();
    let token = $('#txtToken').val();
    
    let reqData = {
        email: email,
        regToken: token
    };

    // Sending request.
    postJson(API_URL + '/user/recovery', reqData, function(err, response) {
        // Stopping loading animation.
        $('#spinnerContainer').css('display', 'none');

        if(err) {
            // Showing error message.
            openMessageModal('Error', err);
            return;
        }

        if(!response.success) {
            openMessageModal('Error', response.message);
            return;
        }

        // Showing success message.
        openMessageModal('Info', 'Your password has been reset. Please check your email to get the new password.');

        messageModalCallback = function() {
            messageModalCallback = null;
            
            // Redirecting to login page.
            window.location = '/login';
        }
    });
});