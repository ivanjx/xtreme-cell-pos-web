// Entry point.
// Checking login status.
getUserInfo(function(err, userInfo) {
    if(!err) {
        // Go to home page.
        window.location = '/home';
    }
});

// Events.
$('#btnRegister').on('click', function() {
    window.location = '/register';
});

$('#frmLogin').on('submit', function(e) {
    // Preventing page reloads.
    e.preventDefault();

    // Showing loading animation.
    $('#spinnerContainer').css('display', 'block');

    // Preparing data.
    let email = $('#txtEmail').val();
    let password = $('#txtPassword').val();
    
    let reqData = {
        email: email,
        plainPassword: password,
        version: VERSION
    };

    // Sending request.
    postJson(API_URL + '/auth/login', reqData, function(err, response) {
        // Hiding loading animation.
        $('#spinnerContainer').css('display', 'none');

        if(err) {
             // Showing error.
             openMessageModal('Error', err);
             return;
        }

        if(!response.success) {
            openMessageModal('Error', response.message);
            return;
        }

        // Login success.
        // Redirecting to home page.
        window.location = '/home';
    });
});