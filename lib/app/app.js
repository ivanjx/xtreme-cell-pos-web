const API_URL = 'https://api.xtremeunlock.com:1000';
const VERSION = '1.0';

const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z$/;

// Global functions.
function jsonDateReviver(key, value) {
    if (typeof value === "string" && dateFormat.test(value)) {
        return new Date(value);
    }
    
    return value;
}

// callback: function(err, response)
function postJson(url, data, callback) {
    $.ajax({
        url: url,
        method: 'POST',
        dataType: 'text',
        contentType: 'application/json',
        data: JSON.stringify(data),
        xhrFields: { withCredentials: true },
        success: function(response) {
            response = JSON.parse(response, jsonDateReviver);
            callback(null, response);
        },
        error: function(err) {
            callback(err, null)
        }
    });
}

// callback: function(err, response)
function getJson(url, params, callback) {
    $.ajax({
        url: url + (params ? '?' + $.param(params) : ''),
        method: 'GET',
        dataType: 'text',
        xhrFields: { withCredentials: true },
        success: function(response) {
            response = JSON.parse(response, jsonDateReviver);
            callback(null, response);
        },
        error: function(err) {
            callback(err, null)
        }
    });
}

// callback: function(err, userInfo)
function getUserInfo(callback) {
    getJson(API_URL + '/user/info', null, function(err, response) {
        if(err) {
            callback(new Error(err));
            return;
        }

        if(!response.success) {
            callback(new Error(response.message), null);
            return;
        }

        callback(null, response.userInfo);
    });
}

function startAutoRefreshToken() {
    getJson(API_URL + '/auth/extend', null, function(err, response) {
        if(err) {
            window.alert('Unable to contact the server.\n' + err);
            return;
        }

        if(!response.success) {
            window.alert('Your session has ended.');
            window.location = '/';
        }

        console.log('Token refreshed successfully');

        // Set timeout for 5 minutes.
        setTimeout(() => {
            startAutoRefreshToken();
        }, 5 * 60 * 1000);
    });
}

function showElement(element) {
    element.css('width', '100%');
    element.css('height', '100%');
    element.css('opacity', '1');
}

function hideElement(element) {
    element.css('width', '0');
    element.css('height', '0');
    element.css('opacity', '0');
}

// Applying message container modal.
$('#msg').html(`
<div id="messageContainer" class="popup-container top-lv-4">
    <div class="popup-content">
        <div class="row">
            <div class="col-md-12 clear-margin-padding">
                <h4><span id="messageContainer-title"></span></h4>
            </div>

            <div class="col-md-12 clear-margin-padding">
                <span id="messageContainer-body"></span>
            </div>

            <div class="col-md-12 clear-margin-padding text-right" style="margin-top: 10px">
                <div class="wrap-panel">
                    <button id="btnCancel_messageContainer"></button>
                    <button id="btnOK_messageContainer">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>
`);

// callback: function(result)
let messageModalCallback = null;

// Message container modal functions.
function openMessageModal(title, message, isPrompt) {
    // Setting dialog.
    if(isPrompt) {
        $('#btnCancel_messageContainer').text('Cancel');
        $('#btnOK_messageContainer').css('display', 'inline-block');
    } else {
        $('#btnCancel_messageContainer').text('Close');
        $('#btnOK_messageContainer').css('display', 'none');
    }

    // Setting message.
    $('#messageContainer-title').text(title);
    $('#messageContainer-body').html(message);

    // Opening modal.
    // We cant use display property.
    // See https://stackoverflow.com/questions/14873031/css-transitions-dont-work-unless-i-use-timeout
    showElement($('#messageContainer'));

    $('#messageContainer .popup-content').css('transform', 'scale(1)');

    // Focusing close button.
    $('#btnCancel_messageContainer').focus();
}

function closeMessageModal(isOk) {
    $('#messageContainer .popup-content').css('transform', 'scale(0)');
    
    setTimeout(() => {
        hideElement($('#messageContainer'));

        // Calling callback.
        if(messageModalCallback) {
            messageModalCallback(isOk);
        }
    }, 300); // 300 ms is the animation time.
}

$('#btnCancel_messageContainer').on('click', function() {
    closeMessageModal(false);
});

$('#btnOK_messageContainer').on('click', function() {
    closeMessageModal(true);
});

// Applying sidebar.
$('.sidebar').html(`
<ul>
    <li>
        <a id="sidebarToggle">
            <div class="sidebar-item-icon fas fa-bars"></div>
        </a>
    </li>
    <hr class="separator" />
    <li>
        <a id="sidebarHome" href="/home">
            <div class="sidebar-item-icon fas fa-home"></div>
            <span class="sidebar-item-text">Home</span>
        </a>
    </li>
    <li>
        <a id="sidebarItem" href="/item">
            <div class="sidebar-item-icon fas fa-list"></div>
            <span class="sidebar-item-text">Item</span>
        </a>
    </li>
    <li>
        <a id="sidebarInventory" href="/inventory">
            <div class="sidebar-item-icon fas fa-table"></div>
            <span class="sidebar-item-text">Inventory</span>
        </a>
    </li>
    <li>
        <a id="sidebarHandler" href="/handler">
            <div class="sidebar-item-icon fas fa-users"></div>
            <span class="sidebar-item-text">Handler</span>
        </a>
    </li>
    <li>
        <a id="sidebarTransaction" href="/transaction">
            <div class="sidebar-item-icon fas fa-dollar-sign"></div>
            <span class="sidebar-item-text">Transaction</span>
        </a>
    </li>
    <li>
        <a id="sidebarPos" href="/pos">
            <div class="sidebar-item-icon fas fa-calculator"></div>
            <span class="sidebar-item-text">POS</span>
        </a>
    </li>
</ul>

<ul class="bottom">
    <li>
        <a id="sidebarLogout">
            <div class="sidebar-item-icon fas fa-power-off"></div>
            <span class="sidebar-item-text">Logout</span>
        </a>
    </li>
</ul>
`);

// Sidebar open.
function openSidebar() {
    $('.sidebar').width(250);
}

function closeSidebar() {
    $('.sidebar').width(48);
}

// Sidebar toggle.
$('#sidebarToggle').on('click', function() {
    let isPaneOpen = $('.sidebar').width() > 48;

    if(isPaneOpen) {
        closeSidebar();
    } else {
        openSidebar();
    }
});

// Sidebar swipe.
$('.sidebar').swipe({
    swipeRight: function(e, direction, distance, duration, fingerCount, fingerData) {
        openSidebar();
    },
    swipeLeft: function(e, direction, distance, duration, fingerCount, fingerData) {
        closeSidebar();
    },
    threshold: 0
});

// Auto close.
$('.sidebar-container').on('click', function() {
    let isPaneOpen = $('.sidebar').width() > 48;

    if(isPaneOpen) {
        closeSidebar();
    }
});

// Logout button.
$('#sidebarLogout').on('click', function() {
    // Sending request.
    getJson(API_URL + '/auth/logout', null, function(err, response) {
        window.location = '/';
    });
});